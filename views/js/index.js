/* ==================== Home page js =========================== */
// Khi load trang
$(document).ready(function () {
    loadMostPopularCourses()
    loadTrendingCourse()
})

// custom function addcourse
$.fn.addCourse = function (paramCourseData) {
    $(this).append(
        `<div class="col-lg-3 col-sm-6 mb-5">
            <div class="card h-100">
                <img src="./${paramCourseData.coverImage}" class="card-img-top" alt="course angular">
                <div class="card-body">
                <h6 class="card-title text-primary">${paramCourseData.courseName}</h6>
                <div class="course-description">
                    <span class="course-duration mr-2"><i class="far fa-clock"></i> ${paramCourseData.duration}</span>
                    <span class="course-level">${paramCourseData.level}</span>
                </div>
                <div class="course-price mt-3">
                    <strong class="course-discount-price">$${paramCourseData.discountPrice ? paramCourseData.discountPrice : paramCourseData.price}</strong>
                    <del class="course-initial-price text-secondary">${paramCourseData.discountPrice ? '$' + paramCourseData.price : ''}</del>
                </div>
                </div>
                <div class="card-footer d-flex align-items-center justify-content-between">
                    <img src="./${paramCourseData.teacherPhoto}" alt="${paramCourseData.teacherName} image" class="rounded-circle" width="40">
                    <small class="pl-2 teacher-name">${paramCourseData.teacherName}</small>
                    <i class="far fa-bookmark"></i>
                </div>
            </div>
        </div>`
    )
}

// Hàm hiển thị most popular courses
function loadMostPopularCourses() {
    var vMostPopularCourses = gCoursesDB.courses.filter(bCourse => bCourse.isPopular)
    vMostPopularCourses.forEach(bCourse => {
        $('#most-popular-courses').addCourse(bCourse)
    })
}

// Hàm hiển thị trending courses
function loadTrendingCourse() {
    var vTrendingCourses = gCoursesDB.courses.filter(bCourse => bCourse.isTrending)
    vTrendingCourses.forEach(bCourse => {
        $('#trending-courses').addCourse(bCourse)
    })
}